#!/usr/bin/env python3
#-*- coding: utf-8 *-*
#by Stephan Fuchs (fuchss@rki.de)

PROGVERSION = '0.0.12'

from collections import defaultdict
import argparse
import re
import os
import gzip
import numpy as np

#FUNCTIONS
def arg():
	parser = argparse.ArgumentParser(prog="fqout", description="provides some fastq statistics")
	parser.add_argument ("--single", help="expect single (non-paired) reads", action = 'store_true')
	parser.add_argument ("--size", metavar="FLOAT", help="calculate fold coverage for expected genome size (in Mb)", type=float, default=False)
	parser.add_argument ("--check", help="perform a basic fastq format check on file(s)", action='store_true')
	parser.add_argument ("-o", "--output", help="name of output file (default: fqstat.txt", type=str, default="fqstat.txt")
	parser.add_argument ("fastq", metavar="FILE", help="gzip-compressed fastq file(s)", type=str, nargs="+")
	parser.add_argument('--version', action='version', version='%(prog)s ' + PROGVERSION)	
	return parser.parse_args()
	
def check_file(*args):
	for arg in args:
		if not os.path.isfile(arg):
			exit("error: file '" + arg + "' not found")

def check_identifier(line, l):
	if line[0] != "@":
		print("format error in line " + str(l) + ": not a valid identifier")

def check_description(line, l):
	if line[0] != "+":
		print("format error in line " + str(l) + ": not a valid description")
		
def check_quality(sline, qline, l):
	if len(sline) != len(qline):
		print("format error in line " + str(l) + ": number of quality values does not match with read length")		
		
def iter_fastq(fname, check=False):
	r = range(3)
	l = 0
	with gzip.open(fname, "rb") as handle:
		while True:
			l += 1
			entry = [handle.readline().decode()]
			if not entry[0]:
				break
			for i in r:
				entry.append(handle.readline().decode())
			if check:
				check_identifier(entry[0], l)
				check_description(entry[2], l+2)
				check_quality(entry[1], entry[3], l+3)
			yield entry
			l += 3

def calc_coverage(bp, target_size_bp):
	return bp/target_size_bp

def main():
	args = arg()
	reads = {}
	output = {}
	cov = {}
	lengths = {}
	gc = {}
	ta = {}
	ns = {}
	
	if os.path.isfile(args.output):
		while True:
			user = input(args.output + " already exists. Should it be overwritten? [Y/N] ").upper()
			if user == "N":
				exit("terminated on user request")
			elif user == "Y":
				os.remove(args.output)
				break
	
	print("checking input ...")
	check_file(*args.fastq)
	
	total = str(len(args.fastq))
	i = 0
	for fastq in args.fastq:
		i += 1
		print ("processing files ... " + "[" + str(i) + "/" + total + "]", end="\r")
		reads[fastq] = 0
		output[fastq] = 0
		lengths[fastq] = []
		gc[fastq] = 0
		ta[fastq] = 0
		ns[fastq] = 0
		for entry in iter_fastq(fastq, args.check):
			reads[fastq] += 1
			seq = entry[1].upper().strip()
			l = len(seq)			
			ta[fastq] += seq.count('T') + seq.count('A') 
			gc[fastq] += seq.count('G') + seq.count('C') 
			ns[fastq] += seq.count('N') 
			lengths[fastq].append(l)
			output[fastq] += l
	print()
	
	print("writing results ...")
	h = ["dataset", "total_reads", "output_bp", "explicit_output_bp", "read_length_mean", "read_length_median", "read_length_sd", "%GC", "%Ns", "Ns"]	
	if args.size:
		size = args.size * 1000000
		h.extend(["fold_coverage__target_size_" + str(args.size) + "Mb", "explicit_fold_coverage__target_size_" + str(args.size) + "Mb"])
	
	
	with open(args.output, "w") as handle:
		if not args.single:
			h.append("files")
			
			fgroups = defaultdict(set)
			for fname in reads.keys():
				fgroup = fname.replace("_R1_", "_R*_").replace("_R2_", "_R*_").replace(".1P.", ".P*.").replace(".2P.", ".P*.").replace(".1U.", ".U*.").replace(".2U.", ".U*.")
				fgroups[fgroup].add(fname)
			
			for fgroup, fnames in fgroups.items():
				if len(fnames)%2 != 0:
					exit("error: number of files assigned to paired dataset '" + fgroup + "' is not equal (n=" + str(len(fgroup)) + ")")
			
			handle.write("\t".join(h) + "\n")
			for fgroup in sorted(fgroups.keys()):
				this_gc = sum([gc[x] for x in fgroups[fgroup]])
				this_ta = sum([ta[x] for x in fgroups[fgroup]])
				this_ns = sum([ns[x] for x in fgroups[fgroup]])
				
				out = [fgroup]
				out.append(sum([reads[x] for x in fgroups[fgroup]])) # total reads
				out.append(sum([output[x] for x in fgroups[fgroup]])) # output
				out.append(this_gc+this_ta) # explicit output
				readl = []
				for fname in fgroups[fgroup]:
					readl.append(lengths[fname])
				out.append(np.mean(readl)) # mean read length
				out.append(np.median(readl)) # median read length
				out.append(np.std(readl)) # std read length
				out.append(round(this_gc/(this_ta+this_gc)*100, 2)) #%GC
				out.append(round(this_ns/out[2]*100, 2)) #%Ns
				out.append(this_ns) #Ns
				if args.size:
					out.append(calc_coverage(out[2], size)) # fold coverage
					out.append(calc_coverage(out[3], size)) # explicit fold coverage
				out.append(", ".join(fgroups[fgroup]))
				handle.write("\t".join([str(x) for x in out]) + "\n")
		else:
			handle.write("\t".join(h))
			for fname in sorted(reads.keys()):
				out = [fname]
				out.append(reads[fname]) # total reads
				out.append(output[fname]) # output
				out.append(ta[fname]+gc[fname]) # explicit output
				out.append(np.mean(lengths[fname])) # mean read length
				out.append(np.median(lengths[fname])) # median read length
				out.append(np.std(lengths[fname])) # std read length
				out.append(round(gc[fname]/(ta[fname]+gc[fname])*100, 2)) #%GC
				out.append(round(ns[fname]/out[2]*100, 2)) #%Ns
				out.append(ns[fname]) #Ns
				if args.size:
					out.append(calc_coverage(out[2], size))
					out.append(calc_coverage(out[3], size))
				handle.write("\t".join([str(x) for x in out]) + "\n")	
	
if __name__ == "__main__":
	main()