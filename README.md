# fqstat 0.0.9

## Description
fqstat writes selected statistics on read number, read length, theoretic coverage, GC and N content into a file.

## 1. Man page
```
usage: fqstat.py [-h] [--single] [--size FLOAT] [--check] [-o OUTPUT]
                 [--version]
                 FILE [FILE ...]

provides some fastq statistics

positional arguments:
  FILE                  gzip-compressed fastq file(s)

optional arguments:
  -h, --help            show this help message and exit
  --single              expect single (non-paired) reads or interleaved file
                        format
  --size FLOAT          calculate fold coverage for expected genome size (in
                        Mb)
  --check               perform a basic fastq format check on file(s)
  -o OUTPUT, --output OUTPUT
                        name of output file (default: fqstat.txt
  --version             show program's version number and exit
```

## 2. Tutorial

### 2.1 Selecting files to process
fqstat accepts GZ-compressed FASTQ-Files only. By default, paired-end read data provided in two files is expected
(see 2.2).
For this, file pairs have to be located in the same folder and have to share the same file name but an additional 
_\_R1\__ (forward reads) or _\_R2\__ (reverse reads) label (e.g. _my_sample_L001_R1_001.fq.gz_ and 
_my_sample_L001_R2_001.fq.gz_). Asterisks are allowed to select multiple files.

Basic command running fqstat on all GZ files (__paired end reads__ in separate files) in the current working directory:
```
fqstat.py *.gz
```

Please consider, that the file names have always to be the last elements of the command.

### 2.2 Process single read data or interleaved formatted data using `--single`
Use the option `--single` to process single read data or paired data stored in one file only (interleaved format).

Command running fqstat on all GZ files (__single reads__) in the current working directory:
```
fqstat.py --single *.gz
```

### 2.3 Calculate theoretic fold coverage using `--size`
You can calculate how often the reads in each dataset will cover a target sequence of a certain length on average.
For this, use the `--size` option followed by the number of megabases of the target molecule.

Example to include calculations of the fold coverage of a 2.8 Mb long target sequence:
```
fqstat.py --size 2.8 *.gz
```

### 2.4 Check fastq file format using `--check`
To perform basic fastq format checks on your files use the `--check` option.

Example to include calculations of the fold coverage of a 2.8 Mb long target sequence:
```
fqstat.py --check *.gz
```

### 2.5 Name output file using `--output`
By default, the output file is named _fqstat.txt_ and stored in the current working directory.
To provide a user-definedname for the output file use the `-o` or `--output` option followed by the 
designated file name.

Example to save results in stats.txt:
```
fqstat.py --output stats.txt *.gz
```

### 2.6 Output
fqstat creates a tab-delimited text file containing following information:

| header | description |
| -------- | -------- |
| dataset   | name of dataset (default) or name of file (if `--single` used)   |
| total_reads   | number of reads   |
| output_bp   | sequencing output in base pairs, considered are all base calls. |
| explicit_output_bp   | sequencing output in base pairs, considered are only explicit base calls [ATGC]. |
| read_length_mean   | mean of all read lengths |
| read_length_median   | median of all read lengths |
| read_length_sd   | standard deviation of all read lengths |
| %GC   | percentage GC content (considered are only explicit base calls [ATGC]) |
| %Ns   | percentage N content |
| Ns   | absolute N content |
| fold_coverage__target_size_*x*Mb  | theoretic fold coverage of a target sequence of _x_ Mb length (if `--size` used)  |
| files | names of files assigned to the respective dataset (if `--single` not used)  |

## Author information
fqstat has been written by Stephan Fuchs, FG13, Robert-Koch-Institute (fuchss@rki.de)
